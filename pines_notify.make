core = 7.x
api = 2

libraries[pnotify][type] = "libraries"
libraries[pnotify][download][type] = "file"
libraries[pnotify][download][url] = "https://github.com/sciactive/pnotify/archive/master.zip"
libraries[pnotify][directory_name] = "pnotify"
libraries[pnotify][destination] = "libraries"
